"""
@author: xuxiangfeng
@date: 2022/2/12
@file_name: query_server.py
"""
import qstock as qs
import re
from flask import Flask, request, render_template

from src.config.common_config import SERVER_IP, SERVER_PORT, PATH, SYMBOL_BASIS_INFO
from src.futures.futures_basis_func import get_futures_basis_info_temp1, cal_fea_single

app = Flask(__name__, template_folder=f"{PATH}/templates")


@app.route('/molin_money_management', methods=["GET"])
def molin_long_time_for_profit():
    ip_port = f"{SERVER_IP}:{SERVER_PORT}"
    return render_template("submit_money_management.html", ip_port=ip_port)


def run_money_management():
    profit = request.form.get('profit', None)
    percent = request.form.get('percent', None)
    symbol = request.form.get('symbol', None)
    stop_loss_cnt = request.form.get('stop_loss_cnt', None)

    # 初始本金
    initial_principal = float(profit) * 10000

    # 单次开仓止损比例
    percent_point = float(percent)

    # 合约代码
    symbol_update = symbol.upper()

    # 品种代码
    symbol_update2 = ''.join(re.findall(r'[A-Za-z]', symbol_update))

    # 默认最大止损几跳
    stop_loss_beat = int(stop_loss_cnt)

    # 合约乘数
    multiplier = SYMBOL_BASIS_INFO.get(symbol_update2).get("multiplier")

    # 合约最小跳动价格
    min_beat_price = SYMBOL_BASIS_INFO.get(symbol_update2).get("min_beat_price")

    # 交易所
    exchange = SYMBOL_BASIS_INFO.get(symbol_update2).get("exchange")

    # 当前价格
    if exchange == "郑商所":
        temp_symbol = symbol_update2 + symbol_update.split(symbol_update2)[1][1:]
    else:
        temp_symbol = symbol_update
    dff = qs.realtime_data(code=temp_symbol)
    now_price = dff.iloc[0]["最新"]

    # 能开仓几手
    res = int(initial_principal * percent_point / 100 / (min_beat_price * stop_loss_beat * multiplier))

    temp_df = get_futures_basis_info_temp1()

    temp_info = temp_df[temp_df["合约代码"] == symbol_update].iloc[0]

    ratio = int(float(temp_info["交易所保证金"]))
    fee_open = cal_fea_single(temp_info["手续费-开仓"], now_price, multiplier)
    fee_today = cal_fea_single(temp_info["手续费-平今"], now_price, multiplier)
    fee_yesterday = cal_fea_single(temp_info["手续费-平昨"], now_price, multiplier)

    # 最大开仓几手
    max_cnt = int(initial_principal / (now_price * multiplier * ratio / 100))

    # 最佳开仓数
    best_cnt = min(max_cnt, res)

    # 计算占用保证金
    aa = now_price * multiplier * ratio / 100 * best_cnt

    result_dic = {
        "best_cnt": best_cnt,
        "max_cnt": max_cnt,
        "fee_open": round(fee_open * best_cnt, 2),
        "fee_today": round(fee_today * best_cnt, 2),
        "fee_yesterday": round(fee_yesterday * best_cnt, 2),
        "aa": round(aa / 10000, 2),
        "aa2": round(aa / initial_principal * 100, 2),
        "now_price": now_price,
        "ratio": ratio,
        "symbol_name": symbol_update
    }
    return render_template("response_money_management.html", **result_dic)


@app.route('/cal_money_management', methods=["POST"])
def cal_long_time_for_profit():
    try:
        return run_money_management()
    except:
        return render_template("error.html")
