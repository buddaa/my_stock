"""
@author: xuxiangfeng
@date: 2022/2/16
@file_name: common_config.py
"""

# 项目目录
PATH = "/xiangfeng/my_stock"

# 服务器公网ip
SERVER_IP = "47.94.99.97"

# 服务的监听端口号
SERVER_PORT = 9999


# 白天可以交易的品种
ALL_FUTURE_SYMBOLS2 = [
    "BU", "FU", "HC", "RB", "C", "CS", "EB", "EG", "JD", "L", "PP", "V", "CF", "CJ",
    "CY", "FG", "MA", "PF", "SA", "SF", "SM", "SR", "UR"]


# 仅仅在白天交易的品种
DAY_ONLY_SYMBOL_LIST = [
    "2年期国债", "10年期国债", "5年期国债", "30年期国债",
    "沪深300股指期货", "中证500股指期货", "中证1000股指期货", "上证50股指期货",
    "尿素", "集运指数(欧线)", "生猪", "苹果", "硅铁", "红枣", "锰硅", "花生",
    "鸡蛋", "纤维板", "线材", "油菜籽", "碳酸锂", "工业硅"]


# 合约乘数和最小跳动价位
SYMBOL_BASIS_INFO = {
    "IM":{"exchange": "中金所", "min_beat_price": 0.2, "multiplier": 200},
    "AG":{"exchange": "上期所", "min_beat_price": 1, "multiplier": 15},
    "AU":{"exchange": "上期所", "min_beat_price": 0.02, "multiplier": 1000},
    "T":{"exchange": "中金所", "min_beat_price": 0.005, "multiplier": 10000},
    "TF":{"exchange": "中金所", "min_beat_price": 0.005, "multiplier": 10000},
    "IF":{"exchange": "中金所", "min_beat_price": 0.2, "multiplier": 300},
    "IC":{"exchange": "中金所", "min_beat_price": 0.2, "multiplier": 200},
    "TL":{"exchange": "中金所", "min_beat_price": 0.01, "multiplier": 10000},
    "RB":{"exchange": "上期所", "min_beat_price": 1, "multiplier": 10},
    "TS":{"exchange": "中金所", "min_beat_price": 0.002, "multiplier": 20000},
    "RU":{"exchange": "上期所", "min_beat_price": 5, "multiplier": 10},
    "P":{"exchange": "大商所", "min_beat_price": 2, "multiplier": 10},
    "SA":{"exchange": "郑商所", "min_beat_price": 1, "multiplier": 20},
    "SC":{"exchange": "能源中心", "min_beat_price": 0.1, "multiplier": 1000},
    "OI":{"exchange": "郑商所", "min_beat_price": 1, "multiplier": 10},
    "AO":{"exchange": "上期所", "min_beat_price": 1, "multiplier": 20},
    "I":{"exchange": "大商所", "min_beat_price": 0.5, "multiplier": 100},
    "M":{"exchange": "大商所", "min_beat_price": 1, "multiplier": 10},
    "HC":{"exchange": "上期所", "min_beat_price": 1, "multiplier": 10},
    "Y":{"exchange": "大商所", "min_beat_price": 2, "multiplier": 10},
    "TA":{"exchange": "郑商所", "min_beat_price": 2, "multiplier": 5},
    "IH":{"exchange": "中金所", "min_beat_price": 0.2, "multiplier": 300},
    "CF":{"exchange": "郑商所", "min_beat_price": 5, "multiplier": 5},
    "RM":{"exchange": "郑商所", "min_beat_price": 1, "multiplier": 10},
    "CU":{"exchange": "上期所", "min_beat_price": 10, "multiplier": 5},
    "FU":{"exchange": "上期所", "min_beat_price": 1, "multiplier": 10},
    "FG":{"exchange": "郑商所", "min_beat_price": 1, "multiplier": 20},
    "SI":{"exchange": "广期所", "min_beat_price": 5, "multiplier": 5},
    "LC":{"exchange": "广期所", "min_beat_price": 50, "multiplier": 1},
    "SN":{"exchange": "上期所", "min_beat_price": 10, "multiplier": 1},
    "LH":{"exchange": "大商所", "min_beat_price": 5, "multiplier": 16},
    "MA":{"exchange": "郑商所", "min_beat_price": 1, "multiplier": 10},
    "PP":{"exchange": "大商所", "min_beat_price": 1, "multiplier": 5},
    "L":{"exchange": "大商所", "min_beat_price": 1, "multiplier": 5},
    "V":{"exchange": "大商所", "min_beat_price": 1, "multiplier": 5},
    "AL":{"exchange": "上期所", "min_beat_price": 5, "multiplier": 5},
    "SR":{"exchange": "郑商所", "min_beat_price": 1, "multiplier": 10},
    "NI":{"exchange": "上期所", "min_beat_price": 10, "multiplier": 1},
    "PG":{"exchange": "大商所", "min_beat_price": 1, "multiplier": 20},
    "EG":{"exchange": "大商所", "min_beat_price": 1, "multiplier": 10},
    "JM":{"exchange": "大商所", "min_beat_price": 0.5, "multiplier": 60},
    "EC":{"exchange": "能源中心", "min_beat_price": 0.1, "multiplier": 50},
    "EB":{"exchange": "大商所", "min_beat_price": 1, "multiplier": 5},
    "NR":{"exchange": "能源中心", "min_beat_price": 5, "multiplier": 10},
    "ZN":{"exchange": "上期所", "min_beat_price": 5, "multiplier": 5},
    "C":{"exchange": "大商所", "min_beat_price": 1, "multiplier": 10},
    "AP":{"exchange": "郑商所", "min_beat_price": 1, "multiplier": 10},
    "PB":{"exchange": "上期所", "min_beat_price": 5, "multiplier": 5},
    "SS":{"exchange": "上期所", "min_beat_price": 5, "multiplier": 5},
    "SM":{"exchange": "郑商所", "min_beat_price": 2, "multiplier": 5},
    "UR":{"exchange": "郑商所", "min_beat_price": 1, "multiplier": 20},
    "SP":{"exchange": "上期所", "min_beat_price": 2, "multiplier": 10},
    "SH":{"exchange": "郑商所", "min_beat_price": 1, "multiplier": 30},
    "PX":{"exchange": "郑商所", "min_beat_price": 2, "multiplier": 5},
    "BU":{"exchange": "上期所", "min_beat_price": 1, "multiplier": 10},
    "BR":{"exchange": "上期所", "min_beat_price": 5, "multiplier": 5},
    "JD":{"exchange": "大商所", "min_beat_price": 1, "multiplier": 10},
    "A":{"exchange": "大商所", "min_beat_price": 1, "multiplier": 10},
    "PF":{"exchange": "郑商所", "min_beat_price": 2, "multiplier": 5},
    "B":{"exchange": "大商所", "min_beat_price": 1, "multiplier": 10},
    "J":{"exchange": "大商所", "min_beat_price": 0.5, "multiplier": 100},
    "SF":{"exchange": "郑商所", "min_beat_price": 2, "multiplier": 5},
    "LU":{"exchange": "能源中心", "min_beat_price": 1, "multiplier": 10},
    "CS":{"exchange": "大商所", "min_beat_price": 1, "multiplier": 10},
    "PK":{"exchange": "郑商所", "min_beat_price": 2, "multiplier": 5},
    "CJ":{"exchange": "郑商所", "min_beat_price": 5, "multiplier": 5},
    "BC":{"exchange": "能源中心", "min_beat_price": 10, "multiplier": 5},
    "RS":{"exchange": "郑商所", "min_beat_price": 1, "multiplier": 10},
    "FB":{"exchange": "大商所", "min_beat_price": 0.5, "multiplier": 10},
    "WR":{"exchange": "上期所", "min_beat_price": 1, "multiplier": 10},
    "BB":{"exchange": "大商所", "min_beat_price": 0.05, "multiplier": 500},
    "CY":{"exchange": "郑商所", "min_beat_price": 5, "multiplier": 5},
    "RR":{"exchange": "大商所", "min_beat_price": 1, "multiplier": 10},
}
