"""
@author: xiangfeng
@date: 2024/5/2
@file_name: run.py
"""
from loguru import logger
import argparse

from src.config.common_config import PATH
from src.stock_01.basic_func import get_all_stock_code, get_stock_recent_100d_kline, is_rise_large_basic_structure, \
    check_back_trade_result


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--test_sdt', type=str)
    args = parser.parse_args()

    logger.add(f"{PATH}/log_files/run_select_stock_{args.test_sdt}.log", retention='10 days')

    all_stock_list = get_all_stock_code()
    stock_num = len(all_stock_list)
    logger.info(f"沪深A股一共有{stock_num}只股票, test_sdt={args.test_sdt}")

    for index, stock_code in enumerate(all_stock_list):
        temp_df = get_stock_recent_100d_kline(test_sdt=args.test_sdt, stock_code=stock_code)
        if temp_df.shape[0] < 100:
            # 该股票上市时间较短，不足100日，不考虑
            continue
        info = is_rise_large_basic_structure(temp_df)
        if isinstance(info, tuple):
            # 该股票符合上升大基本结构
            stop_loss_price, open_position_price, target_amplitude = info
            new_stop_loss_price = max(open_position_price * 0.95, stop_loss_price)
            logger.info(f"stock_code={stock_code} | 开仓价={open_position_price}, 止损价={stop_loss_price}, 调整后的止损价={round(new_stop_loss_price, 2)}, AB幅度={round(target_amplitude, 2)}")
            result = check_back_trade_result(stock_code, args.test_sdt, stop_loss_price, open_position_price, target_amplitude)
            if result != 0:
                logger.info(f"stock_code={stock_code}, profit={result}")

        if index % 500 == 0:
            logger.info(f"当前进度：[{index} / {stock_num}]")

