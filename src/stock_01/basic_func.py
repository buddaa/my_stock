"""
@author: xiangfeng
@date: 2024/5/2
@file_name: basic_func.py
"""
from typing import List
import pandas as pd
import qstock as qs
from datetime import datetime, timedelta
from loguru import logger


def get_all_stock_code() -> List[str]:
    """
    获取沪深A股的所有股票code
    """
    df = qs.realtime_data('沪深A')
    df["target"] = df["名称"].apply(lambda x: "ST" in x)
    df2 = df[(df["成交量"] > 0) & (df["target"] == False)]
    return list(df2["代码"])


def get_stock_recent_100d_kline(test_sdt, stock_code: str) -> pd.DataFrame:
    """
    获取某只股票最近100根日K线的数据
    """
    test_dt = datetime.strptime(test_sdt, "%Y%m%d")
    start_sdt = (test_dt - timedelta(days=200)).strftime("%Y%m%d")
    df = qs.get_data(stock_code, start=start_sdt, end=test_sdt, freq='d', fqt=0)
    df2 = df.tail(100)
    df3 = df2[["open", "high", "low", "close", "volume"]].reset_index()
    return df3


def is_rise_large_basic_structure(basis_df: pd.DataFrame):
    """
    判断该股票当下是否符合上升大基本结构
    """
    # ma30 = basis_df[-30:]["close"].mean()
    # now_price = basis_df.iloc[-1]["close"]
    # if now_price <= ma30:
    #     # 当下的收盘价在30日均线以下，不考虑开仓
    #     return False

    temp_df = basis_df.sort_values("low")
    lowest_price = float(temp_df.iloc[0]["low"])
    lowest_price_index = list(temp_df.iloc[0:1].index)[0]

    temp_df2 = basis_df.sort_values("high")
    highest_price = float(temp_df2.iloc[-1]["high"])
    highest_price_index = list(temp_df2.iloc[-1:].index)[0]

    if lowest_price_index >= highest_price_index:
        # 此时处于下跌中，不可能形成上升大基本结构
        return

    basis_df2 = basis_df[basis_df.index > highest_price_index]
    if basis_df2.shape[0] < 1:
        # 上升大基本结构还没有形成
        return

    temp_df3 = basis_df2.sort_values("low")

    # 最高价之后的最低价
    lower_price = float(temp_df3.iloc[0]["low"])
    lower_price_index = list(temp_df3.iloc[0:1].index)[0]

    """
    O = (0, )
    A = (lowest_price_index, lowest_price)
    B = (highest_price_index, highest_price)
    C = (lower_price_index, lower_price)
    """
    time_AB = highest_price_index - lowest_price_index
    time_BC = lower_price_index - highest_price_index

    con1 = 10 < time_AB < 60
    con2 = 1 < time_BC / time_AB < 5
    con3 = lowest_price_index / time_AB > 2

    volume_OA = sum(basis_df.iloc[: lowest_price_index]["volume"])
    volume_AB = sum(basis_df.iloc[lowest_price_index: highest_price_index]["volume"])
    volume_BC = sum(basis_df.iloc[highest_price_index: lower_price_index]["volume"])

    con4 = True if volume_OA == 0 else volume_AB / volume_OA > 2
    con5 = volume_AB / volume_BC > 2

    p_AB = 2 * (highest_price - lowest_price) / (lowest_price + highest_price)
    p_BC = 2 * (highest_price - lower_price) / (highest_price + lower_price)

    con6 = 0.3 < p_AB < 1.2
    con7 = p_BC / p_AB < 0.65

    if con1 and con2 and con6 and con7:
        return lower_price, highest_price, highest_price - lowest_price
    return


def check_back_trade_result(stock_code, test_sdt, stop_loss_price, open_position_price, target_amplitude):
    """
    回测系统
    stop_loss_price: 止损价位
    open_position_price: 开仓价位
    target_amplitude: AB的幅度
    """
    test_dt = datetime.strptime(test_sdt, "%Y%m%d")
    start_sdt = (test_dt + timedelta(days=1)).strftime("%Y%m%d")
    end_sdt = (test_dt + timedelta(days=30)).strftime("%Y%m%d")
    df = qs.get_data(stock_code, start=start_sdt, end=end_sdt, freq='d', fqt=0)
    df2 = df[["high", "low"]].reset_index()

    # 当前账户状态（0: 未开仓; 1: 满仓; 2: 半仓; 3: 已清仓）
    status = 0

    # 账户初始余额为100万
    balance = 1000000

    # 最多能开几手
    full_hand_cnt = int(balance / (100 * open_position_price))

    new_stop_loss_price = max(open_position_price * 0.95, stop_loss_price)

    # 先平掉一半仓位的止盈位
    sell_half_price = 2 * open_position_price - new_stop_loss_price

    # 最后平掉剩下一半仓位的止盈位
    sell_full_price = new_stop_loss_price + target_amplitude

    result = []
    for _, row in df2.iterrows():
        if status == 0 and row["low"] < new_stop_loss_price:
            # 还没有开仓，价格已经跌破C点
            # logger.info(f"stock_code={stock_code}, 还没有开仓，价格已经跌破C点")
            break

        if status == 0 and row["low"] < open_position_price < row["high"]:
            status = 1
            result.append([row["date"], "开仓", 0])
            continue

        if status == 1 and row["low"] < new_stop_loss_price:
            # 止损平仓
            status = 3
            temp0 = -100 * full_hand_cnt * (open_position_price - new_stop_loss_price)
            result.append([row["date"], "止损平仓", temp0])
            break

        if status == 1 and row["high"] > sell_half_price:
            # 先平掉一半的仓位
            status = 2
            temp1 = full_hand_cnt // 2 * 100 * (open_position_price - new_stop_loss_price)
            result.append([row["date"], "平半仓", temp1])
            continue

        if status == 2 and row["low"] < open_position_price:
            # 另一半仓位平价出来
            status = 3
            result.append([row["date"], "平另一半仓", 0])
            break

        if status == 2 and row["high"] > sell_full_price:
            # 另一半仓位止盈出来
            status = 3
            temp3 = (full_hand_cnt - full_hand_cnt // 2) * 100 * (sell_full_price - open_position_price)
            result.append([row["date"], "平另一半仓", temp3])

    if status == 0:
        logger.info(f"stock_code={stock_code}, 该股票没有开仓的机会")
        return 0.0

    tt = pd.DataFrame(result, columns=["date", "action", "profit"])
    logger.info(f"""
    stock_code={stock_code}, 最多能开{full_hand_cnt}手, 持仓明细如下:\n{tt}""")
    return float(tt["profit"].sum())




