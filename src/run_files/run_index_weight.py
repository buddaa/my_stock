"""
@author: xuxiangfeng
@date: 2024/1/25
@file_name: run_index_weight.py
"""
from loguru import logger
import akshare as ak

from src.config.common_config import PATH
from src.utils.message_utils import my_send_email


def get_index_weight_df(index_code):
    """
    000001  上证指数
    (399001  深证成指)
    (399006  创业板指)
    000300  沪深300
    000852  中证1000指数
    """
    df = ak.index_stock_cons_weight_csindex(symbol=index_code)
    df2 = df.sort_values("权重", ascending=False).reset_index()
    df3 = df2[["成分券名称", "成分券代码", "权重", "日期"]]
    return df3


if __name__ == '__main__':
    try:
        df_001 = get_index_weight_df("000001")
        df_001.to_excel(f"{PATH}/log_files/上证指数的股票权重.xlsx", header=True, index=False, encoding='utf-8-sig')

        df_300 = get_index_weight_df("000300")
        df_300.to_excel(f"{PATH}/log_files/沪深300的股票权重.xlsx", header=True, index=False, encoding='utf-8-sig')

        df_852 = get_index_weight_df("000852")
        df_852.to_excel(f"{PATH}/log_files/中证1000的股票权重.xlsx", header=True, index=False, encoding='utf-8-sig')
    except Exception as e:
        logger.exception(e)
        my_send_email("更新股票权重报错", "定时更新【股票权重】失败", "buddaa@foxmail.com")
