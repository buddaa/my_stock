"""
@author: xiangfeng
@date: 2024/6/1
@file_name: fix_holiday_cjl.py

对假期的成交量做修正处理
"""
import pandas as pd
from datetime import datetime

from src.config.common_config import PATH


if __name__ == '__main__':
    now_sdt = datetime.now().strftime('%Y%m%d')

    if now_sdt in ("20240611", "20240918", "20241008"):
        exchange_23h_df = pd.read_excel(f"{PATH}/log_files/所有合约在23点收盘后的情况.xlsx")
        exchange_09h_df = pd.read_excel(f"{PATH}/log_files/所有合约在9点前收盘后的情况.xlsx")

        exchange_23h_df.loc[:, ["成交量(21-23)", "成交额(21-23)"]] = 0
        exchange_09h_df.loc[:, ["成交量(21-09)", "成交额(21-09)"]] = 0

        exchange_23h_df.to_excel(f"{PATH}/log_files/所有合约在23点收盘后的情况.xlsx", header=True, index=False,
                                 encoding='utf-8-sig')
        exchange_09h_df.to_excel(f"{PATH}/log_files/所有合约在9点前收盘后的情况.xlsx", header=True, index=False,
                                 encoding='utf-8-sig')

